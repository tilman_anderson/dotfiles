#!/bin/sh

# export LIBVA_DRIVER_NAME=nvidia
export XDG_SESSION_TYPE=wayland
# export GBM_BACKEND=nvidia-drm
# export __GLX_VENDOR_LIBRARY_NAME=nvidia
# export WLR_NO_HARDWARE_CURSORS=1

export TERMINAL="foot"

export XDG_CURRENT_DESKTOP=Hyprland
# export XDG_SESSION_TYPE=wayland
export XDG_SESSION_DESKTOP=Hyprland

export QT_AUTO_SCREEN_SCALE_FACTOR=1
export QT_QPA_PLATFORM="wayland;xcb"
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1

# export SDL_VIDEODRIVER=wayland
export MOZ_ENABLE_WAYLAND=1
export CLUTTER_BACKEND="wayland"
export GDK_BACKEND="wayland,x11"

id_amd="$(lspci | grep -E "(VGA|3D)" | awk '/Radeon/ {print $1}')"
id_intel="$(lspci | grep -E "(VGA|3D)" | awk '/Intel/ {print $1}')"

card_amd="$(readlink /dev/dri/by-path/pci-0000:"$id_amd"-card | sed 's|..|/dev/dri|')"
card_intel="$(readlink /dev/dri/by-path/pci-0000:"$id_intel"-card | sed 's|..|/dev/dri|')"

# DRI_PRIME=1
WLR_DRM_DEVICES="$card_amd":"$card_intel" dbus-run-session Hyprland
